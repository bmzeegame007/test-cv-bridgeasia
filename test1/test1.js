
$('document').ready(function() {
    async function main() {
        await setLisPost();
    }
    main();
});

function getjsonAuthors() {
    $.getJSON('https://bridgeasiagroup.com/baRes/authors.json', function(data) {
        dataAuthors = data;
    
    });
}

function getjsonPost() {
    $.getJSON('https://bridgeasiagroup.com/baRes/posts.json', function(data) {
        dataPosts = data
    });
}

function setLisPost(){
    var dataPosts;
    var dataAuthors;
    async function main() {
        await $.getJSON('https://bridgeasiagroup.com/baRes/authors.json', function(data) {
            dataAuthors = data
        });

        await $.getJSON('https://bridgeasiagroup.com/baRes/posts.json', function(data) {
            dataPosts = data
        });

        await displayShow(dataAuthors,dataPosts);
    }
    main();
}


function displayShow(user, posts){
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    let text = "";
        for (let i = 0; i < posts.length; i++) {
            var event = new Date(posts[i].created_at)
            text += '<div class="card mb-3">\
                        <div class="row g-0">\
                            <div class="col-md-2">\
                                <img id="picPost'+ i +'" class="img-fluid rounded-start" src = "'+ posts[i].image_url +'">\
                            </div>\
                            <div class="col-md-8">\
                                <div class="card-body">\
                                    <h5 class="card-title" id="titlePost'+ i +'">'+ posts[i].title +'</h5>\
                                    <p class="card-text textPost" id="textPost'+ i +'">'+ posts[i].body +'</p>\
                                    <p class="card-text datePost" id="datePost'+ i +'"><small class="text-muted">'+ event.toLocaleDateString("en-US", options) +'</small></p>\
                                </div>\
                            </div>';
            for(let j = 0; j < user.length; j++){
                if(posts[i].author_id == user[j].id){
                    text += '<div class="col-md-2 postionimg">\
                                <img id="picAuthor'+ i +'" class="imguser" src="'+ user[j].avatar_url +'">\
                                <div class="postiontext">\
                                    <p class="card-text nameAuthor" id="nameAuthor'+ i +'">'+ user[j].name +'</p> \
                                    <p class="card-text posAuthor" id="posAuthor'+ i +'">'+ user[j].role +'</p> \
                                    <p class="card-text liveAuthor" id="liveAuthor'+ i +'">'+ user[j].place +'</p> \
                                </div>\
                            </div>\
                        </div>\
                    </div>';
                    break;
                }
            }
            
        }
        document.getElementById("ui").innerHTML = text;
}
